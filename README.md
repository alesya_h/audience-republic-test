# audience-republic-test

This repository contains an implementation of random directed weighted graph generation,
Dijkstra algorithm for finding shortest paths, functions to provide some graph metrics,
and some additional code to visualise graphs and radius/diameter paths on them.


## Usage

To see the code in action you may want to run `(audience-republic-test.core/demo)` from the REPL.
It will display a graph and print it's metrics in the console.


## Code structure

The code is organised in namespaces under audience-republic-test.graph:

* generate - functions to generate a random graph
* traverse.shortest-path-first - Dijkstra algorithm
* metrics - excentricity, radius, diameter metrics

Additionally there are

* render - visualise a graph and optionally some paths on them via graphviz/dot
* utils - shared graph utility functions
* traverse.bfs-dfs - copy of the original bfs/dfs code for unweighted graphs from the task

To run the tests, run `lein test` in the root of the repository.


## Notes about development process

First I decided on the data format - I used maps for edge targets and weights instead of a vector with lists.

Then I started writing graph generation in a single namespace with an editor-connected REPL.

Then I added Dijkstra algorithm and metrics. I had some type errors that I could have caught if I used clojure.spec
from the beginning, but at that point it was easier to track and fix them manually. That was the case because the algorithm was known, and the set of necessary operations was limited, so investment in specs wouldn't pay off.

After that I added graph visualisation to make it easier to visually check if the code behaves correctly. I also exploded the code into namespaces.

After that I added tests. Tests are using clojure.test and are written using with-test, as their primary use case is documenting how the code is used and what data it returns. As I was adding tests, I removed code snippets that I used testing those functions in the REPL, and intermediate prints. Current tests are not exhaustive, and if this was a very critical code, I'd use specs, and probably would opt for generative testing.


## License

Copyright © 2020 Ales Huzik

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

This Source Code may also be made available under the following Secondary
Licenses when the conditions for such availability set forth in the Eclipse
Public License, v. 2.0 are satisfied: GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or (at your
option) any later version, with the GNU Classpath Exception which is available
at https://www.gnu.org/software/classpath/license.html.
