# A basic image editor #

## Introduction ##

One of the areas that Audience Republic takes great pride in is in great design. We spend a lot of money and time on designs and take pride in the way our users interact with our product.

You can see an example of the campaigns that we run for fans here:

* https://arep.co/p/ar-syd-presale

### Effects ###

As you've most likely encountered in your front end development work, sometimes patchy browser support means that we've had to implement some effects _manually_.

One place where we had to to do this was in the background Gaussian blur'ed image in the background of the campaign, which you can see if you look back at the campaign [Pleasure Garden](https://arep.co/p/the-pleasure-garden-presale-example).

The blur in the background image is first created by loading the foreground image into a HTML canvas and then blurring it by applying a manual Gaussian blur and setting the background image to the new image. This was done for Safari because at the time it did not support the HTML5 canvas `blur` property.

## Task ##

I'd like you to do a little experiment with HTML5 canvas.

This link contains a png showing a screen design for the 

[Screen design](https://drive.google.com/open?id=1nrA_v8P1SmOQjGKEkoXpHqO8mzOp6AFC)

and the Sketch file can be found here:

[Sketch design](https://drive.google.com/file/d/1PV5wtOWZ283cgi-LPSYoYC8zC2SYrP4U/view?usp=sharing)

The screen design shows the layout of a tiny webapp that allows the user too:

1. Load and display an image file (png or jpg) from the filesystem (using a normal file picker dialog) by clicking on the bottom-right hand "upload" button. Pictured in the design is an image of a tent that the user has previously uploaded (by clicking on the upload button); the image should be resized so that the horizontal width is the same as the image element. Keep the aspect ratio the same, it doesn't matter if the vertical dimension does not fill the entire height of the image element,
2. Once the image has been loaded, the horizontal brightness/contrast sliders become active,
3. The user can adjust the brightness and contrast of the image, in *real time*, by moving the slider left or right from its centre position to decrease or increase brightness/contrast respectively. NOTE: By "real time" we mean as the slider is dragged, do not wait for a mouse up event to trigger the change in brightness or contrast, update it as the slider is dragged,
4. Replace the little picture of the man with a nice picture of yourself! :)

Use this file: 

[Test image](https://drive.google.com/open?id=1aYj1fJyw_RosxqjNSB7NWABFNHXfmgye)

for testing.

### What I'll look for: ###

1. Attention to detail, it's a very simple web application, so you should be able to design it so it looks exactly like what I've pictured + maybe some additional highlights that fit within the general theme,
2. Attention to code layout and separation of concerns,
3. You're creating UI components & following the Atomic design pattern.
4. **You're manipulating the pixel values directly**. I realise that HTML5 canvas contains `blur` and `contrast` properties, but this exercise is to show that you can also reason about iterating over pixel values and manipulating them appropriately.
5. It's important that contrast and brightness _compose_, by that I mean when you adjust the brightness and then adjust the contrast, the contrast adjustment should be applied over the brightness adjustment.

### Framework ###

Audience Republic works almost exclusively with `Vue.js` and we intend to only be using `Vue.js` in the near future. 

As such the solution that you send back should be done within the `Vue.js` framework.

You can use this project: https://github.com/vuejs/vue-cli to help you start a new Vue.js project and then add too it. If you've not used Vue.js before, the documentation: https://vuejs.org/

## Deliverables ##

1. Link to demonstration page in which I can upload a file and test brightness and contrast functionality, I will be checking for *pixel perfect* CSS
2. Link to github (other code repo site) I can download the code and build locally to test and review. **Make sure you include instructions on how to build and server in a README file.**