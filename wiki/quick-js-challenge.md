# Quick JS challenge

## Puzzle Game

This game is to get a better idea of your understanding of Javascript (JS) & Promises execution flow.

We have given an example of a JS function under Puzzle #1 below and then written out the execution flow (as a function of time) as if we had executed this JS function. The answer to Puzzle #1 can be read as "run() is called, once it finishes executing, it returns a promise, and then runAnotherFuction() is called and finally, once that is done, the function handler(..) is called".

Given the answer to Puzzle #1 please sketch out the flow of execution for the other remaining puzzles.

#### Puzzle #1

```js
run()
.then(function () {
  return runAnotherFunction();
})
.then(handler);
```

Answer:
```
run
|-----------------|
                  runAnotherFunction(undefined)
                  |------------------|
                                     handler(resultOfrunAnotherFunction)
                                     |------------------|
--------------------------------------------------------> (time)
```

#### Puzzle #2
```js
run()
.then(function () {
  runAnotherFunction();
})
.then(handler);
```

#### Puzzle #3
```js
run()
.then(runAnotherFunction())
.then(handler);
```

#### Puzzle #4
```js
run()
.then(runAnotherFunction)
.then(handler);
```

## Quick challenge

Using the students array below, write a javascript function to return an object containing:

- The name of the class as the key.
- The total attended lessons for each class.
- The average amount of attended lessons for each class.

`students.json`
```json
{
  "students": [
    {
      "name": "Lulu Gearside",
      "class": "art",
      "attended": 35
    },
    {
      "name": "Matthew Milham",
      "class": "art",
      "attended": 11
    },
    {
      "name": "Dany Dufner",
      "class": "biology",
      "attended": 12
    },
    {
      "name": "Jeremy Doyle",
      "class": "biology",
      "attended": 3
    },
    {
      "name": "Tim O'Connor",
      "class": "biology",
      "attended": 10
    },
    {
      "name": "Charlie Wang",
      "class": "french",
      "attended": 12
    }
  ]
}
```

Expected output:

```js
{
  "art": {
    "total": 46,
    "average": 23,
  },
  "biology": {
    "total": 25,
    "average": 8,
  },
  "french": {
    "total": 12,
    "average": 12,
  },
}
```