(ns audience-republic-test.graph.utils
  (:require [clojure.test :as t]))



(t/with-test
  (defn edge-set [graph]
    (->> graph
         (mapcat (fn [[source targets]]
                   (map (fn [[target weight]]
                          [source target weight])
                        targets) ))
         set))

  (t/is (= (edge-set {:1 {:2 1, :3 2}, :2 {:1 4}, :3 {}})
           #{[:1 :2 1] [:1 :3 2] [:2 :1 4]})))



(t/with-test
  (defn path-to-edge-seq [path]
    (map vector path (rest path)))

  (t/is (= (path-to-edge-seq [1 2 3])
           [[1 2] [2 3]])))
