(ns audience-republic-test.graph.metrics
  (:require
   [audience-republic-test.graph.traverse.shortest-path-first :as spf]
   [clojure.test :as t]))


(t/with-test
  (defn excentricity [graph node]
    (->> (spf/shortest-path-first graph node)
         (map (fn [[k v]] (:distance v)))
         (reduce max)))

  ;; binary was easier for me here because then distance bits reveal paths taken
  (let [graph {:1 {:2 2r00001, :3 2r00010},
               :2 {:4 2r00100},
               :3 {:4 2r01000},
               :4 {:1 2r10000}}]
    (t/is (= (excentricity graph :1) 2r00101))   ; 1 -> 2 -> 4
    (t/is (= (excentricity graph :2) 2r10110))   ; 2 -> 4 -> 1 -> 3
    (t/is (= (excentricity graph :3) 2r11001))   ; 3 -> 4 -> 1 -> 2
    (t/is (= (excentricity graph :4) 2r10010)))) ; 4 -> 1 -> 3


(defn- all-excentricities [graph]
  (->> (keys graph) (map #(excentricity graph %))))

(defn diameter [graph]
  (->> (all-excentricities graph) (reduce max)))

(defn radius [graph]
  (->> (all-excentricities graph) (reduce min)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn excentricity-with-targets [graph node]
  (->> (spf/shortest-path-first graph node)
       (map (fn [[k v]] {:target k :distance (:distance v)}))
       (reduce (fn [current new] (if (< (:distance current) (:distance new)) new current)))))

(defn all-excentricities-vec [graph]
  (->> (keys graph)
       (map #(assoc (excentricity-with-targets graph %) :source %))
       (sort-by :distance)
       vec))

(defn source-target-map-to-path [graph {:keys [source target]}]
  (spf/find-shortest-path graph source target))

(defn radius-path [graph]
  (some->> (all-excentricities-vec graph) first (source-target-map-to-path graph)))

(defn diameter-path [graph]
  (some->> (all-excentricities-vec graph) peek (source-target-map-to-path graph)))
