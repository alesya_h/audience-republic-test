(ns audience-republic-test.graph.traverse.shortest-path-first
  "Implements Dijkstra algrorithm to find shortest paths.

  We track distance information in a priority map and update it as we visit nodes with shortest distance.
  In the beginning every node is infinitely far, except the start node.
  On each step we
    1. take the closest unprocessed node (on the first step this is the start node)
    2. use it to update distance information
    3. remove the node from the set of unprocessed nodes

  "
  (:require [clojure.data.priority-map :as pm]
            [clojure.test :as t]))



(t/with-test
  (defn make-targets-data-patch [targets-data edges-to-process current-node current-distance]
    (->> edges-to-process
         (mapcat (fn [[target-node distance-from-current-node]]
                   (let [distance-via-current-node (+ distance-from-current-node current-distance)]
                     (if (< (-> targets-data target-node :distance) distance-via-current-node)
                       []
                       [[target-node {:distance distance-via-current-node :previous current-node}]]))))
         (into {})))

  (t/is
   (= (make-targets-data-patch {:1 {:distance 1 :previous :0} :2 {:distance 1000 :previous :1}},
                               {:1 3 :2 4}, :42, 100)
      {:2 {:distance 104 :previous :42}}))) ;; unlike to node :1, distance to node :2 is shorter from
                                            ;; the current node :42 than the one we knew previously



(defn update-targets-data [graph targets-data current-node unprocessed-nodes]
  (let [current-distance (get-in targets-data [current-node :distance])
        edges-to-process (select-keys (get graph current-node) unprocessed-nodes)
        new-targets-data-via-this-node (make-targets-data-patch targets-data edges-to-process current-node current-distance)]
    (into targets-data new-targets-data-via-this-node))) ;; step 2, see ns doc



(t/with-test
  (defn shortest-path-first [graph source-node]
    (let [all-nodes (keys graph)]
      (loop [unprocessed-nodes (set all-nodes)
             targets-data (-> all-nodes
                              (->> (map #(vector % {:distance ##Inf :previous nil}))
                                   (into (pm/priority-map-by #(< (:distance %) (:distance %2)))))
                              (assoc-in [source-node :distance] 0))]
        (if (empty? unprocessed-nodes)
          targets-data
          (let [next-node (some (fn [[k v]] (get unprocessed-nodes k)) targets-data) ;; step 1, see ns doc
                new-targets-data (update-targets-data graph targets-data next-node unprocessed-nodes)]
            (recur (disj unprocessed-nodes next-node) new-targets-data)))))) ;; step 3, see ns doc

  (t/is (= (shortest-path-first {:1 {:2 1, :3 2},
                                 :2 {:4 4},
                                 :3 {:4 2},
                                 :4 {}} :1)
           {:1 {:distance 0, :previous nil}
            :2 {:distance 1, :previous :1}
            :3 {:distance 2, :previous :1}
            :4 {:distance 4, :previous :3}})))



(t/with-test
  (defn find-shortest-path [graph source target]
    (let [targets-data (shortest-path-first graph source)]
      (when (-> targets-data (get target) :distance (not= ##Inf))
        (loop [current-node target
               path (list target)]
          (let [{:keys [distance previous] :as x} (get targets-data current-node)]
            (if previous
              (recur previous (conj path previous))
              path))))))

  (t/is (= (find-shortest-path {:1 {:2 1, :3 2},
                                :2 {:4 4},
                                :3 {:4 2},
                                :4 {}}
                               :1 :4)
           [:1 :3 :4])))
