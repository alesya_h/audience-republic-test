(ns audience-republic-test.graph.generate
  "
  This namespace contains functions for generating random connected weighted graphs.

  Graph is represented as a nested map such that
    (get-in graph [source-node-id target-node-id]) ; => weight if the edge exists, nil otherwise
  Nodes that don't have outgoing edges still should be present as keys in the toplevel map.

  Example graph:
  {:1 {:2 1, :3 2},
   :2 {:4 4},
   :3 {:4 2},
   :4 {}}

  Example edge set:
  #{[:1 :2 1] [:1 :3 2] [:2 :4 4] [:3 :4 2]}


  "
  (:require
   [clojure.set :as s]
   [clojure.test :as t]
   [audience-republic-test.graph.utils :as gu]))



(t/with-test
  (defn make-random-nodes [n-nodes]
    (->> (range n-nodes) (map inc) (map str) (map keyword) (into #{})))

  (let [nodes (make-random-nodes 5)]
    (t/is (every? keyword? nodes))
    (t/is (= (count nodes) 5))))



(t/with-test
  (defn ensure-node [graph node]
    (update graph node #(or % {})))

  (let [graph {:a {:b 1 :c 2}, :b {}, :c {:a 3}}]
    (t/is (contains? (ensure-node graph :c) :c))
    (t/is (contains? (ensure-node graph :d) :d))
    (t/is (= (get (ensure-node graph :d) :d) {}))))



(defn ensure-edge [graph [source target] weight]
  (assoc-in graph [source target] weight))



(t/with-test
  (defn rand-int-in-range [i-min i-max]
    (+ i-min (rand-int (inc (- i-max i-min)))))

  (t/is (->> (repeatedly #(rand-int-in-range 3 9))
             (take 30)
             (every? #(<= 3 % 9)))))



(defn random-edge-weight []
  (rand-int-in-range 1 10))



(t/with-test
  (defn add-and-randomly-connect-node [graph new-node]
    (if-some [existing-nodes (keys graph)]
      (let [connection-node (rand-nth existing-nodes)]
        (-> graph
            (ensure-node new-node)
            (ensure-edge (shuffle [new-node connection-node]) (random-edge-weight))))
      {new-node {}}))

  (let [graph {:1 {:2 1, :3 2}, :2 {:1 4}, :3 {}}
        new-graph (add-and-randomly-connect-node graph :4)
        new-edges (s/difference (gu/edge-set new-graph) (gu/edge-set graph))
        [source target weight] (first new-edges)]
    (t/is (= 1 (count new-edges)))
    (t/is (or (= source :4) (= target :4)))
    (t/is (= {:1 {}} (add-and-randomly-connect-node {} :1)))))



(defn add-and-randomly-connect-nodes [graph nodes]
  (if (empty? nodes)
    graph
    (recur (add-and-randomly-connect-node graph (first nodes)) (rest nodes))))



(t/with-test
  (defn available-source-nodes
    "Available source nodes are those that have fewer outgoing links than there are other nodes in total"
    [graph]
    (let [n-other-nodes (dec (count graph))]
      (filter #(< (-> graph (get %) count) n-other-nodes)
              (keys graph))))

  (t/is (= (set (available-source-nodes {:1 {:2 1, :3 2},
                                         :2 {:1 4},
                                         :3 {}}))
           #{:2 :3})))



(t/with-test
  (defn available-target-nodes [graph source-node]
    (let [all-nodes-set (set (keys graph))
          existing-target-nodes (keys (get graph source-node))]
      (s/difference all-nodes-set existing-target-nodes #{source-node})))

  (t/is (= #{:3} (set (available-target-nodes {:1 {:2 1, :3 2}, :2 {:1 4}, :3 {}} :2))))
  (t/is (= #{:1 :2} (set (available-target-nodes {:1 {:2 1, :3 2}, :2 {:1 4}, :3 {}} :3)))))



(t/with-test
  (defn add-random-edge [graph]
    (let [source-nodes (available-source-nodes graph)
          _ (assert (seq source-nodes) "Can't create new edge. All nodes are connected.")
          source-node (rand-nth source-nodes)
          target-node (rand-nth (seq (available-target-nodes graph source-node)))]
      (ensure-edge graph [source-node target-node] (random-edge-weight))))

  (let [graph {:1 {:2 1, :3 2}, :2 {:1 4}, :3 {}}
      new-graph (add-random-edge graph)
      new-edges (s/difference (gu/edge-set new-graph) (gu/edge-set graph))]
    (t/is (= 1 (count new-edges)))))



(defn add-random-edges [graph n-extra-edges]
  (if (zero? n-extra-edges)
    graph
    (recur (add-random-edge graph) (dec n-extra-edges))))



(t/with-test
  (defn make-random-graph [n-nodes n-edges]
    ;; todo: add argument validation
    ;; note: n-edges constraint (from N-1 to N(N-1)/2) seems incorrect,
    ;; as it's a constraint for an UNDIRECTED connected graph
    (let [nodes (make-random-nodes n-nodes)
          base-graph (add-and-randomly-connect-nodes {} nodes)
          n-extra-edges (- n-edges (dec n-nodes))]
      (assert (not (neg-int? n-extra-edges)) "Not enough edges to make a connected graph")
      (add-random-edges base-graph n-extra-edges)))
  (let [graph (make-random-graph 4 8)]
    (t/is (= 4 (count (keys graph))))
    (t/is (= 8 (count (gu/edge-set graph))))))
