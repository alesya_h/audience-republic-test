(ns audience-republic-test.graph.render
  (:require
   [audience-republic-test.graph.utils :as u]
   [dorothy.core :as dot]
   [dorothy.jvm :as dotj]))

(defn make-main-edge-seq [graph]
  (->> graph u/edge-set seq
       (mapcat (fn [[source target weight]]
                 [ (dot/edge-attrs {:label (str weight)}) [source target] ]))))

(defn make-path-edge-seq [{:keys [attrs path]}]
  (when (seq path)
    (cons (dot/edge-attrs attrs) (u/path-to-edge-seq path))))

(defn show! [graph & paths]
  (-> graph
      make-main-edge-seq
      vec
      (concat (mapcat make-path-edge-seq paths))
      dot/digraph
      dot/dot
      dotj/show!
      ))
