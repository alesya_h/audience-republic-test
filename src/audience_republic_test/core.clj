(ns audience-republic-test.core
  (:require
   [audience-republic-test.graph.generate :as gen]
   [audience-republic-test.graph.metrics :as m]
   [audience-republic-test.graph.render :as render]))

(defn demo []
  (let [g (gen/make-random-graph 5 9)
        radius-path (m/radius-path g)
        diameter-path (m/diameter-path g)]
    (prn {:diameter (m/diameter g)
          :radius (m/radius g)})
    (render/show! g
                  {:attrs {:color "red" :label ""} :path radius-path}
                  {:attrs {:color "blue" :label ""} :path diameter-path})))

;(demo)
